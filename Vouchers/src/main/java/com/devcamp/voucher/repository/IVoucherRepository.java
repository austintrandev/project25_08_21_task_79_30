package com.devcamp.voucher.repository;

import com.devcamp.voucher.model.CVoucher;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
	@Transactional
	@Modifying
	@Query(value = "UPDATE p_vouchers SET phan_tram_giam_gia = :phanTram WHERE ma_voucher  = :ma_voucher", nativeQuery = true)
	int updatePhanTram(@Param("ma_voucher") String maVoucher, @Param("phanTram") String phanTram);
}
