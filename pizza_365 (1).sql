-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 25, 2021 at 07:18 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza_365`
--

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` bigint(20) NOT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `country_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `country_code`, `country_name`) VALUES
(1, 'VN101', 'Việt Nam'),
(2, 'JP102', 'Japan'),
(3, 'KR103', 'Korea');

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `p_vouchers`
--

CREATE TABLE `p_vouchers` (
  `id` bigint(20) NOT NULL,
  `ghi_chu` varchar(255) DEFAULT NULL,
  `ma_voucher` varchar(255) NOT NULL,
  `ngay_cap_nhat` datetime DEFAULT NULL,
  `ngay_tao` datetime DEFAULT NULL,
  `phan_tram_giam_gia` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `p_vouchers`
--

INSERT INTO `p_vouchers` (`id`, `ghi_chu`, `ma_voucher`, `ngay_cap_nhat`, `ngay_tao`, `phan_tram_giam_gia`) VALUES
(1, NULL, '16512', '2021-08-19 10:05:39', '2021-08-19 10:06:01', '20'),
(2, NULL, '12354', '2021-08-19 10:10:34', '2021-08-19 10:10:34', '10'),
(3, '', '12332', '2021-08-19 10:05:21', '2021-08-19 10:12:34', '10'),
(4, NULL, '12333', '2021-08-19 10:10:56', '2021-08-19 10:10:56', '10'),
(5, NULL, '95531', '2021-08-19 10:11:05', '2021-08-19 10:11:05', '10'),
(6, NULL, '81432', '2021-08-19 10:11:16', '2021-08-19 10:11:16', '10'),
(7, NULL, '15746', '2021-08-19 10:11:30', '2021-08-19 10:11:30', '10'),
(8, NULL, '76241', '2021-08-19 10:11:41', '2021-08-19 10:11:41', '10'),
(9, NULL, '64562', '2021-08-19 10:11:52', '2021-08-19 10:11:52', '10'),
(10, NULL, '25896', '2021-08-19 10:12:02', '2021-08-19 10:12:02', '10');

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE `region` (
  `id` bigint(20) NOT NULL,
  `region_code` varchar(255) DEFAULT NULL,
  `region_name` varchar(255) DEFAULT NULL,
  `country_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`id`, `region_code`, `region_name`, `country_id`) VALUES
(1, 'HN', 'Hà nội', 1),
(2, 'HCM', 'Hồ Chí Minh', 1),
(3, 'DN', 'Đà Nẵng', 1),
(4, 'TOK', 'Tokyo', 2),
(5, 'KOB', 'Kobe', 2),
(6, 'OSA', 'Osaka', 2),
(7, 'SEO', 'Seoul', 3),
(8, 'BUS', 'Busan', 3),
(9, 'JEJ', 'Jeju', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_oqixmig4k8qxc8oba3fl4gqkr` (`country_code`);

--
-- Indexes for table `p_vouchers`
--
ALTER TABLE `p_vouchers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_s5e2tmxq8xpr2dro60iuirr01` (`ma_voucher`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_is5udyhip6itt1wt8tj8hx5wq` (`region_code`),
  ADD KEY `FK7vb2cqcnkr9391hfn72louxkq` (`country_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `region`
--
ALTER TABLE `region`
  ADD CONSTRAINT `FK7vb2cqcnkr9391hfn72louxkq` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
